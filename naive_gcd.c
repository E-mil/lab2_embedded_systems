#include <stdio.h>
//#include "getnum.c"

unsigned getnum() {
    unsigned int num;
    scanf("%d", &num);
    return num;
}

unsigned gcd(int a, int b);

int main(void) {
    unsigned short N = getnum()-1;
    unsigned short i;
    int a, b;
    a = getnum();
    for (i = 0; i < N; i++) {
        b = getnum();
        a = gcd(a, b);
    }
    //xil_printf("gcd = %d\n", a);
    printf("gcd = %d\n", a);
    return 0;
}

unsigned gcd(int a, int b) {
    if (a == 0) {
        return b;
    } else if (b == 0) {
        return a;
    }
    int n;
    if (a < b) {
        n = a;
    } else {
        n = b;
    }
    while (a % n != 0 || b % n != 0) {
        n -= 1;
    }
    return n;
}