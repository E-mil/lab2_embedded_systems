#include "xuartlite_l.h"
#include "xparameters.h"

unsigned getnum() {
    char srb=0;
    unsigned num=0;

    // skip non digits
    while(srb < '0' || srb > '9') srb=XUartLite_RecvByte(STDIN_BASEADDRESS);

    // read all digits
    while(srb >= '0' && srb <= '9') { 
        num=num*10+(srb-'0');
        srb=XUartLite_RecvByte(STDIN_BASEADDRESS);
    };
    return num;
}