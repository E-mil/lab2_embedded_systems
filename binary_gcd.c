#include <stdio.h>
//#include "getnum.c"

unsigned getnum() {
    unsigned int num;
    scanf("%d", &num);
    return num;
}

unsigned gcd(int a, int b);

int main(void) {
    unsigned short N = getnum()-1;
    unsigned short i;
    int a, b;
    a = getnum();
    for (i = 0; i < N; i++) {
        b = getnum();
        a = gcd(a, b);
    }
    //xil_printf("gcd = %d\n", a);
    printf("gcd = %d\n", a);
    return 0;
}

unsigned gcd(int a, int b) {
    if (a == 0) {
        return b;
    }
    if (b == 0) {
        return a;
    }
    while (a != b) {
        if (a & 1) { // a is odd
            if (b & 1) { // b is odd
                if (a < b) {
                    b = (b - a) >> 1;
                } else {
                    a = (a - b) >> 1;
                }
            }
        } else { // a is even
            a = a >> 1;
        }
        if (~b & 1) { // b is even
            b = b >> 1;
        }
    }
    return a;
}